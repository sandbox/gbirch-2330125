/**
 * Attaches the calendar behavior to all required fields
 */
(function ($) {
Drupal.behaviors.simple_date = {
  attach: function (context) {
    if ( window.matchMedia("(max-width: 42em)").matches ) {
      for (var id in Drupal.settings.simpleDatePopup) {
        var $input = $('#' + id);
        var initialDate = $input.val();
        if (initialDate) {
          var timestamp = Date.parse(initialDate);
          $input.val($.datepicker.formatDate('yy-mm-dd', new Date(timestamp)));
        }
        $input[0].type = 'date'; // Can't use .attr() because jQuery doesn't allow you to change "type".
      }
    } else {
      for (var id in Drupal.settings.simpleDatePopup) {
        $('#'+ id).bind('focus', Drupal.settings.simpleDatePopup[id], function(e) {
          if (!$(this).hasClass('date-popup-init')) {
            var simpleDatePopup = e.data;
            $(this)
              .datepicker(simpleDatePopup.settings)
              .addClass('date-popup-init')
            $(this).click(function(){
              $(this).focus();
            });
          }
        });
      }
    }
  }
};
})(jQuery);
