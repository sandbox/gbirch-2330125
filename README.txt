Drupal simple_date_popup.module README.txt
==============================================================================

Javascript popup calendar using the jquery UI calendar.  This is based on the
date_popup submodule of Date, but is somewhat simpler to use.  It produces
a standard Drupal text field input instead of the ornate fieldset produced by date.
It is also somewhat more forgiving about input, and will actually TRY to construct a
valid date. Time selection is NOT supported. [TODO See the note below about UTC. which will
often mean that the date stored in the database is actually different from the local
date due to timezone conversion.  (12:00 am in Moscow is still the day before in Greenwich).]

================================================================================
Datepicker
================================================================================

This code uses the jQuery UI datepicker that is included in core. Localization
of the interface is handled by core.

The popup will use the site default for the first day of the week.

TODO:  Add usage section.
